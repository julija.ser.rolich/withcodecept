Feature('Logout');

Scenario('Пользователь может выйти из системы, по нажатию иконки close в верхнем меню сайта', ({ I, mainPage, logoutPage }) => {
    I.amOnPage('/');
    mainPage.login('demo','demo');
    mainPage.confirmLogin();
    logoutPage.closeBank();
    I.see('Войти');
});
