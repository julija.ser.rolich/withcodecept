Feature('Setting');

Scenario.skip('Пользователь может перейти в Настройки интернет-банкинга и выбрать раздел с Гос.Услугами', ({ I, settingsPage,  mainPage }) => {
    I.amOnPage('/');
    mainPage.login('demo','demo');
    mainPage.confirmLogin();
    settingsPage.allSettings();
    settingsPage.personalSetting();
    settingsPage.ibankSettings();
    settingsPage.selectGosuslugi();
    I.seeElement('#settings-container > #settings-data-container > #esia-intro #esia-logo');
});

Scenario.skip('Пользователь может перейти в Настройки, где по умолчаению раздел Аватары с информ.сообщением', ({ I, settingsPage,  mainPage }) => {
    I.amOnPage('/');
    mainPage.login('demo','demo');
    mainPage.confirmLogin();
    settingsPage.allSettings();
    settingsPage.personalSetting();
    I.see('body > #settings-container > #settings-data-container > #settings-form > .alert');
});
