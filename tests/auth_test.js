Feature('Auth');

Scenario.skip('Пользователь может авторизоваться в интернет-банкинге, введя логин/пароль', ({ I , mainPage }) => {
    I.amOnPage('/');
    mainPage.login('demo','demo');
    mainPage.confirmLogin();
    I.see('Hello');
});

Scenario.skip('Пользователь видит pop-up с информацией, по нажатию кнопки Восстановить доступ', ({ I , mainPage }) => {
    I.amOnPage('/');
    mainPage.forget();
    I.see('Забыли логин или пароль?');
});

