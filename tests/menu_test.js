Feature('Menu');

Scenario.skip('Пользователь видит Приветствие в верхнем меню сайта', ({ I, mainPage, menuPage }) => {
    I.amOnPage('/');
    mainPage.login('demo','demo');
    mainPage.confirmLogin();
    I.see('Hello World!');
});

Scenario.skip('Имеется элемент c логотипом Банка, по нажатию лого в верхнем меню сайта', ({ I, mainPage, menuPage }) => {
    I.amOnPage('/');
    mainPage.login('demo','demo');
    mainPage.confirmLogin();
    menuPage.logoCheck();
    I.seeElementInDOM('#logo');
});

Scenario.skip('Пользователь может увидеть переписку с банком, по нажатию иконки e-mail в верхнем меню сайта', ({ I, mainPage, menuPage }) => {
    I.amOnPage('/');
    mainPage.login('demo','demo');
    mainPage.confirmLogin();
    menuPage.iconEmailCheck();
    I.see('Переписка с банком');
});

Scenario.skip('Пользователь может увидеть персональные предложения, по нажатию иконки candy в верхнем меню сайта', ({ I, mainPage, menuPage }) => {
    I.amOnPage('/');
    mainPage.login('demo','demo');
    mainPage.confirmLogin();
    menuPage.iconBadgeCheck();
    I.see('Персональные предложения');
});

Scenario.skip('Пользователь может перейти на страницу отправки сообщения банку, по нажатию иконки phone в верхнем меню сайта', ({ I, mainPage, menuPage }) => {
    I.amOnPage('/');
    mainPage.login('demo','demo');
    mainPage.confirmLogin();
    menuPage.iconPhoneCheck();
    I.see('Новое сообщение');
});




