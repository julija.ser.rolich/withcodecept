const { I } = inject();

module.exports = {
    fields: {
        username: '.container > #login-container > #login-form > .form-control:nth-child(2) > input',
        password: '.container > #login-container > #login-form > .form-control:nth-child(3) > input',
    },
    button: {
        login: '.container > #login-container > #login-form #login-button',
        doublerLogin: 'body #login-otp-button',
        forgetPass: '.container > #login-container > #login-form > #additional-actions > .chevron',
    },

    login(username, password) {
        I.fillField(this.fields.username, username);
        I.fillField(this.fields.password, password);
        I.click(this.button.login);
    },

    confirmLogin(){
        I.click(this.button.doublerLogin);
    },

    forget(){
        I.click(this.button.forgetPass);
    }
};
