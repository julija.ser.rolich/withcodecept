const { I } = inject();

module.exports = {
    button:{
        settings: '#controls > #user > #preference-controls > #settings-button > .icon-cog',
        personalSet: '#body > #contentbar > .nav > .active > a',
        ibankSet: '#body > #contentbar > .nav > li:nth-child(2) > a',
        gosuslugi: '#settings-container > .span3 > .nav > li:nth-child(2) > a',
        avatars: '#settings-container > .span3 > .nav > .active > a',

    },

    allSettings(){
        I.click(this.button.settings);
    },

    personalSetting(){
        I.click(this.button.personalSet);
    },

    ibankSettings(){
        I.click(this.button.ibankSet);
    },

    selectGosuslugi(){
        I.click(this.button.gosuslugi);
    },

};
