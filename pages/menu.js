const { I } = inject();

module.exports = {
    button: {
        logo: '#inner-wrapper > #header > .content > #logo > .print-hidden',
        greeting: '.content > #controls > #user #user-greeting',
        iconEmail: '#controls > #user > #preference-controls > #messages-button > .icon-email',
        iconBadge: '#controls > #user > #preference-controls > #offers-button > .icon-candy',
        iconPhone: '#controls > #user > #preference-controls > #contact-button > .icon-phone',
        wrightToBank: '.logged-in > #default-dialog > .modal-body > #contacts > .btn',
    },

    logoCheck(){
        I.click(this.button.logo);
    },

    iconEmailCheck(){
        I.click(this.button.iconEmail);
    },

    iconBadgeCheck(){
        I.click(this.button.iconBadge);
    },

    iconPhoneCheck(){
        I.click(this.button.iconPhone);
        I.click(this.button.wrightToBank);
    },

};
