const { I } = inject();

module.exports = {
    button: {
        logout: '.content > #controls > #user > #logout-button > .icon-close'
    },

    closeBank(){
        I.click(this.button.logout);
    }
};
